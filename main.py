import pandas
from sklearn.ensemble import RandomForestClassifier
from sklearn.cross_validation import ShuffleSplit
import numpy as np

val_fraction = 0.05

def prepare_data(dataframe, train):
    data = dataframe.fillna(9999)

    grouped_data = data.groupby('VisitNumber')

    grouped_fineline_numbers = grouped_data['FinelineNumber'].apply(lambda x: x.tolist())
    num_trips = len(grouped_fineline_numbers)

    fineline_ids = data['FinelineNumber'].unique()
    fineline_id_mapping = {int(fid): idx for idx, fid in enumerate(fineline_ids)}
    num_fineline_ids = len(fineline_id_mapping)

    unique_trip_types = data['TripType'].unique()
    trip_type_mapping = {t: idx for idx, t in enumerate(unique_trip_types)}

    num_trip_types = len(trip_type_mapping)

    print('num fineline ids: {}, num trip types: {}'.format(num_fineline_ids, num_trip_types))

    X = np.zeros((num_trips, num_fineline_ids))
    y = np.zeros((num_trips, num_trip_types))
    dataframe_indexes = np.zeros(num_trips)

    trip_types = grouped_data['TripType'].apply(lambda x: x.tolist())

    for X_index, entry in enumerate(grouped_fineline_numbers.iteritems()):
        index, fineline_numbers_list = entry
        X_entry = []
        for num in fineline_numbers_list:
            X[X_index, fineline_id_mapping[int(num)]] = 1
        trip_type = trip_types[index][0]
        y[X_index, trip_type_mapping[trip_type]] = 1
        dataframe_indexes[X_index] = index

    return X, y, trip_type_mapping, fineline_id_mapping

if __name__ == '__main__':
    data = pandas.read_csv('data/part_train.csv')
    test_data = pandas.read_csv('data/part_test.csv')
    data.info()

    X, y, trip_type_mapping, fineline_id_mapping = prepare_data(data, True)
    reverse_trip_type_mapping = {idx: t for t, idx in trip_type_mapping.items()}

    print('built dataset, start training')

    num_trips = X.shape[0]
    print(num_trips, X.shape)

    for train_idxs, test_idxs in ShuffleSplit(num_trips, n_iter=1, test_size=val_fraction, random_state=2):
        X_train = X[train_idxs]
        y_train = y[train_idxs]
        X_test = X[test_idxs]
        y_test = y[test_idxs]

        clf = RandomForestClassifier(n_jobs=-1, verbose=1, random_state=2)
        clf.fit(X_train, y_train)
        print('val score', clf.score(X_test, y_test))

        pred_values = y_test.argmax(1)
        result = clf.predict(X_test)
        print(result.argmax(1))
        print(pred_values)
        #print(dataframe_indexes[test_idxs])
        print(reverse_trip_type_mapping)



    with open('result.csv', 'w') as res_file:
        head_string = 'VisitNumber'
        for i in sorted(unique_trip_types):
            head_string += ',TripType_' + str(i)
        res_file.write(head_string + '\n')
        for i in range(y_test.shape[0]):
            pred_value = pred_values[i]
            pred_trip_type = trip_type_mapping[pred_value]
            trip_type_entry = str(test_dataframe_indexes[i])
            trip_type_result = [0] * num_trip_types
            trip_type_result[pred_value] = 1
            trip_type_entry += ',' + ','.join(trip_type_result)







